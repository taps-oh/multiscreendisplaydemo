export default class Util {
  private progressInterval: number = undefined
  private progressValue: number = 0

  private constructor() {
  }

  public static getInstance(): Util {
    return globalThis.MUtil ??= new Util()
  }

  public startProgressInterval() {
    this.clearProgressInterval()
    this.progressInterval = setInterval(() => {
      if (this.progressValue >= 100) {
        clearInterval(this.progressInterval)
      } else {
        this.progressValue++
      }
      AppStorage.SetOrCreate<number>('progressValue', this.progressValue)
    }, 150)
  }

  public clearProgressInterval() {
    if (this.progressInterval) {
      clearInterval(this.progressInterval)
      this.progressValue = 0
      AppStorage.SetOrCreate<number>('progressValue', this.progressValue)
    }
  }
}