import HiLog from '@ohos.hilog'

export default class Log {
  private static readonly DOMAIN = 0x0304
  private static readonly TAG: string = '[MultiScreenDisplayDemo]'
  public static readonly LEVEL_DEBUG = 0;
  public static readonly LEVEL_LOG = 1;
  public static readonly LEVEL_INFO = 2;
  public static readonly LEVEL_WARN = 3;
  public static readonly LEVEL_ERROR = 4;
  //    public static LOG_LEVEL = Log.LEVEL_LOG;
  public static LOG_LEVEL = -1;

  public static debug(message: string) {
    if (this.LOG_LEVEL <= this.LEVEL_DEBUG) {
      HiLog.debug(this.DOMAIN, this.TAG, message)
    }
  }

  public static log(message: string) {
    if (this.LOG_LEVEL <= this.LEVEL_LOG) {
      HiLog.info(this.DOMAIN, this.TAG, message)
    }
  }

  public static info(message: string) {
    if (this.LOG_LEVEL <= this.LEVEL_INFO) {
      HiLog.info(this.DOMAIN, this.TAG, message)
    }
  }

  public static warn(message: string) {
    if (this.LOG_LEVEL <= this.LEVEL_WARN) {
      HiLog.warn(this.DOMAIN, this.TAG, message)
    }
  }

  public static error(message: string) {
    if (this.LOG_LEVEL <= this.LEVEL_ERROR) {
      HiLog.error(this.DOMAIN, this.TAG, message)
    }
  }
}