import display from '@ohos.display'

export default class Constant {
  public static readonly SUB_WINDOW_WIDTH = 600
  public static readonly SUB_WINDOW_HEIGHT = 500

  public static readonly MAIN_WINDOW_PREFIX = 'customMainWindow_'
  public static readonly SUB_WINDOW_PREFIX = 'customSubWindow_'
  public static readonly ENABLE_MULTI_SCREEN_NAME = 'persist.display.expand.enabled'
  public static readonly DEFAULT_START_BUNDLE_NAME = 'com.ohos.settings'
  public static readonly DEFAULT_START_ABILITY_NAME = 'com.ohos.settings.MainAbility'

  public static readonly DEFAULT_UNSELECT_WINDOW_PAGE = '------'
  public static readonly DEFAULT_MAIN_WINDOW_PAGE = 'pages/DefaultMainWindowPage'

  public static readonly CONFIG = {
    IS_CREATE_MAIN_WINDOW: false //是否为副屏创建主桌面窗口(launcher代码已合入，在launcher中为副屏创建主桌面窗口)
  }
}

class DisplayPage {
  public pagePaths: Array<string> = [] //显示屏上创建的子窗口内容界面路径
  public display: display.Display = undefined //已连接的显示屏

  constructor(display: display.Display, pagePaths: Array<string> = []) {
    this.display = display
    this.pagePaths = pagePaths
  }
}

class WindowPage {
  public pagePath: string
  public showingDisplayId: number

  constructor(pagePath: string, showingDisplayId: number = -1) {
    this.pagePath = pagePath
    this.showingDisplayId = showingDisplayId
  }
}

export { DisplayPage, WindowPage }